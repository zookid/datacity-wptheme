<?php
/**
 * Understrap enqueue scripts
 *
 * @package understrap
 */

if ( ! function_exists( 'understrap_scripts' ) ) {
	/**
	 * Load theme's JavaScript sources.
	 */
	function understrap_scripts() {
		// Get the theme data.
		$the_theme = wp_get_theme();
		wp_enqueue_style( 'understrap-styles', get_stylesheet_directory_uri() . '/css/theme.min.css', array(), $the_theme->get( 'Version' ) );
		wp_enqueue_style( 'custom-styles', get_stylesheet_directory_uri() . '/css/custom.css', array(), $the_theme->get( 'Version' ) );
		wp_enqueue_style( 'articles-styles', get_stylesheet_directory_uri() . '/css/articles.css', array(), $the_theme->get( 'Version' ) );
		wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri() . '/css/font-awesome/css/font-awesome.min.css', array(), $the_theme->get( 'Version' ) );
		wp_enqueue_style( 'font-abel', 'https://fonts.googleapis.com/css?family=Abel', array(), $the_theme->get( 'Version' ), true );
		wp_enqueue_script( 'jquery');
		wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), true);
		wp_enqueue_script( 'understrap-scripts', get_template_directory_uri() . '/js/theme.min.js', array(), $the_theme->get( 'Version' ), true );
		wp_enqueue_script( 'googleMaps', get_template_directory_uri() . '/js/googleMaps.js', array(), $the_theme->get( 'Version' ), true );
		wp_enqueue_script( 'menuToggle', get_template_directory_uri() . '/js/menuToggle.js', array(), $the_theme->get( 'Version' ), true );
		wp_enqueue_script( 'smoothScroll', get_template_directory_uri() . '/js/smoothScroll.js', array(), $the_theme->get( 'Version' ), true );
		wp_enqueue_script( 'printArticle', get_template_directory_uri() . '/js/printArticle.js', array(), $the_theme->get( 'Version' ), true );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // endif function_exists( 'understrap_scripts' ).

add_action( 'wp_enqueue_scripts', 'understrap_scripts' );
