<nav class="navbar navbar-custom <?php echo is_home() ? 'position-absolute w-100' : 'navbar-regular' ?> navbar-expand-lg">
  <a class="navbar-brand navbar-logo" href="<?php echo get_home_url() ?>">
    <img src="<?php echo get_template_directory_uri(); ?>/img/logo-datacity.png" class="img-fluid" alt="Datacity">
  </a>
  <button
    id="menuToggler"
    class="navbar-toggler"
    type="button"
    data-toggle="collapse"
    data-target="#navbarMenu"
    aria-controls="navbarMenu"
    aria-expanded="false"
    aria-label="Toggle navigation">
      <img 
        src="<?php echo get_template_directory_uri(); ?>/img/icono-menu.png"
        class="navbar-icon"
        alt="menu">
      MENU
  </button>
  <div 
    class="collapse navbar-collapse d-lg-flex justify-content-end" 
    id="navbarMenu">
    <ul class="navbar-nav">
      <li class="nav-item d-lg-none">
        <a href="<?php echo get_page_link( get_page_by_path( 'soluciones' ) )?>" class="navbar-link">
          SOLUCIONES
        </a>
      </li>
      <li class="nav-item d-lg-none">
        <a href="<?php echo get_page_link( get_page_by_path( 'nosotros' ) ); ?>" class="navbar-link">
          NOSOTROS
        </a>
      </li>
      <li class="nav-item d-lg-none">
        <a href="<?php echo get_page_link( get_page_by_path( 'participamos' ) ); ?>" class="navbar-link">
          PARTICIPAMOS
        </a>
      </li>
      <li class="nav-item d-lg-none">
        <a href="<?php echo get_page_link( get_page_by_path( 'proyectos' ) )?>" class="navbar-link">
          PROYECTOS
        </a>
      </li>
      <?php if (is_home()): ?>
      <li id="telefono" class="nav-item d-flex mt-3 mt-lg-0 pr-4 pr-lg-0">
        <div class="pt-2 pt-lg-0 pr-2">
          <img src="<?php echo get_template_directory_uri(); ?>/img/icono-phone.png" 
            class="navbar-icon" 
            alt="Telefono">
        </div>
        <div>
          <span class="mr-2 mr-lg-0 d-block d-lg-inline">
            +562 29935452
          </span>
          <span class=" d-block d-lg-inline">
            +562 29935453
          </span>
        </div>
      </li>
      <?php endif; ?>
      <li class="nav-item my-1 my-lg-0">
        <div class="navbar-social">
          <a target="_blank" href="https://www.facebook.com/DatacityIoT/" class="navbar-link">
            <img src="<?php echo get_template_directory_uri(); ?>/img/icono-facebook.png" 
              class="navbar-icon" 
              alt="facebook">
          </a>
          <a target="_blank" href="https://twitter.com/DatacityIoT" class="navbar-link">
            <img 
              src="<?php echo get_template_directory_uri(); ?>/img/icono-twitter.png" 
              class="navbar-icon" 
              alt="twitter">
          </a>
          <a target="_blank"  href="https://www.linkedin.com/company/datacity-iot/" class="navbar-link">
            <img 
              src="<?php echo get_template_directory_uri(); ?>/img/icono-linkedin.png" 
              class="navbar-icon" 
              alt="linkedin">
          </a>
          <a target="_blank" href="https://www.instagram.com/datacityiot/" class="navbar-link">
            <img 
              src="<?php echo get_template_directory_uri(); ?>/img/icono-instagram.png" 
              class="navbar-icon" 
              alt="instagram">
          </a>
        </div>
      </li>
      <?php if (is_home() == false): ?>
        <li class="nav-item">
          <a href="<?php echo get_page_link(get_page_by_path('buscar'));?>" class="navbar-link">
            BUSCADOR
          </a>
        </li>
      <?php endif; ?>
      <li class="nav-item">
        <a href="<?php echo get_page_link( get_page_by_path( 'contacto' ) ); ?>" class="navbar-link">
          CONTACTO
        </a>
      </li>
      <li class="nav-item" id="menu">
        <button class="btn-none" 
        type="button" 
        id="dropdownMenuButton" 
        data-toggle="dropdown" 
        aria-haspopup="true" 
        onclick="openNav()"
        aria-expanded="false">
          <img
            src="<?php echo get_template_directory_uri(); ?>/img/icono-menu.png"
            class="navbar-icon"
            alt="menu">
            <span id="navbar-menu-title">
              MENU
            </span>
        </button>
      </li>
      <li class="nav-item">
        <a href="#" class="xx navbar-link">
          <?php echo do_shortcode('[language-switcher]'); ?>
        </a>
      </li>
    </ul>
  </div>
  </nav>

  <div id="sidebar" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <a href="<?php echo get_page_link( get_page_by_path( 'soluciones' ) )?>" class="navbar-link">
      SOLUCIONES
    </a>
    <a href="<?php echo get_page_link( get_page_by_path( 'nosotros' ) ); ?>" class="navbar-link">
      NOSOTROS
    </a>
    <a href="<?php echo get_page_link( get_page_by_path( 'participamos' ) ); ?>" class="navbar-link">
      PARTICIPAMOS
    </a>
    <a href="<?php echo get_page_link( get_page_by_path( 'proyectos' ) )?>" class="navbar-link">
      PROYECTOS
    </a>
    <div class="sidenav-social">
      <a target="_blank" href="https://www.facebook.com/DatacityIoT/">
        <img src="<?php echo get_template_directory_uri(); ?>/img/icono-facebook.png" 
          class="" 
          alt="facebook">
      </a>
      <a target="_blank" href="https://twitter.com/DatacityIoT">
        <img 
          src="<?php echo get_template_directory_uri(); ?>/img/icono-twitter.png" 
          class="" 
          alt="twitter">
      </a>
      <a target="_blank"  href="https://www.linkedin.com/company/datacity-iot/">
        <img 
          src="<?php echo get_template_directory_uri(); ?>/img/icono-linkedin.png" 
          class="" 
          alt="linkedin">
      </a>
      <a target="_blank" href="https://www.instagram.com/datacityiot/">
        <img 
          src="<?php echo get_template_directory_uri(); ?>/img/icono-instagram.png" 
          class="" 
          alt="instagram">
      </a>
    </div>
  </div>
