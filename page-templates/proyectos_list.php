<?php
/**
 * Template Name: proyectos_lista
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>
<section id="top">
    <?php get_template_part( 'global-templates/topnav' ); ?>
</section>

<?php
if( !empty($_GET['filtro']) ) $tag=$_GET['filtro'];
else $tag = '';
$proyectos_query = new WP_Query( array( 
  'category_name' => 'proyectos',  
  'tag' => $tag,
  'order' => 'DESC',
));
?>

<section id="proyectos" d="top">
  <h1 class="bg-black">Proyectos</h1>
  <h3>
    <a class="<?php echo $tag == '' ? 'active' : ''; ?>" 
      href="?filtro=">
      - TODAS
    </a>
    <a class="<?php echo $tag == 'u-dacity-smart-pole' ? 'active' : ''; ?>" 
      href="?filtro=u-dacity-smart-pole">
      - U-DACITY SMART POLE
    </a>
    <a class="<?php echo $tag == 'smart-hub-satt' ? 'active' : ''; ?>" 
      href="?filtro=smart-hub-satt">
      - SMART HUB SATT
    </a>
    <a class="<?php echo $tag == 'smart-warning-system' ? 'active' : ''; ?>" 
      href="?filtro=smart-warning-system">
      - SMART WARNING SYSTEM -
    </a>
  </h3>
  <div id="proyectos-list" class="container-fluid">
    <div class="row h-100">
      <?php if ( $proyectos_query->have_posts() ) : ?>
        <?php 
        $n = 0;
        while ( $proyectos_query->have_posts() ) : $proyectos_query->the_post(); ?>
          <div class="col-md-3 thumb <?php echo $n %2 == 0 ? 'ph-gray' : 'ph-darkgray'; ?>"
          style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full')[0]; ?>)">
            <a href="<?php echo the_permalink(); ?>">
              <?php the_title(); ?>
            </a>
          </div>
        <?php $n++; endwhile; ?>
        <?php wp_reset_postdata(); ?>
      <?php else: ?>
        <h3 class="mx-auto text-gray">
          No hay proyectos que mostrar
        </h3>
      <?php endif; ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>