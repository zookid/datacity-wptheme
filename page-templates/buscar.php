<?php
/**
 * Template Name: buscar
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>
<section id="top">
    <?php get_template_part( 'global-templates/topnav' ); ?>
</section>

<div class="main-wrapper position-relative">
  <div class="main">
    <img class="mx-auto d-block" 
      src="<?php echo get_template_directory_uri(); ?>/img/logo-datacity-favicon.png" 
      class="img-fluid">
    <h3>Busca en los posts de Datacity</h3>
    <div class="box">
      <?php get_template_part( 'searchform' ); ?>
    </div>
  </div>
</div>

<?php get_footer(); ?>