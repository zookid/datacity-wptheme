<?php
  get_header();
  $container = get_theme_mod( 'understrap_container_type' );
?>

<section id="top" class="d-print-none">
  <?php get_template_part( 'global-templates/topnav' ); ?>
</section>

<section>
  <h1 class="bg-blue">
    <a class="nostyle" href="<?php echo get_page_link(get_page_by_path('participamos')); ?>">Participamos</a>
  </h1>
  <div class="container paragraph py-4 article-head">
    <!-- navegacion -->
    <div class="d-sm-flex justify-content-between text-center d-print-none">
      <?php previous_post_link( '%link', '< NOTICIA ANTERIOR', TRUE ); ?>
      <?php next_post_link( '%link', 'SIGUIENTE NOTICIA >', TRUE ); ?>
    </div>
    <!-- titulo y subtitulo del post -->
    <h3 class="text-left py-4">
      <?php while ( have_posts() ) : the_post(); ?>
        <?php the_title(); ?>
      <?php endwhile;?>
      <div class="subtitle-caption mt-1 text-lightblue text-uppercase">
        <?php if (get_field('subtitulo')): ?>
          <?php the_field('subtitulo') ?>
        <?php endif; ?>
      </div>
    </h3>
    <hr>
    <!-- perfil del autor -->
    <div class="d-sm-flex justify-content-between">
      <div class="autor-info">
        <!-- avatar del autor -->
        <p class="author_details">
          <?php echo get_avatar( get_the_author_meta('user_email') , 90 ) ?>
        </p>
        <div class="autor-bio">
          <span>
            Por
            <b class="text-capitalize">
              <!-- nombre completo -->
              <?php while ( have_posts() ) : the_post(); ?>
                <?php echo get_the_author_meta('first_name')." ".get_the_author_meta('last_name'); ?>
              <?php endwhile;?>
            </b>
          </span>
          <!-- biografia/cargo, la informacion que aparece ahi -->
          <p class="text-gray">
            <?php echo get_the_author_meta( 'user_description', $post->post_author ); ?>
          </p>
        </div>
      </div>
      <!-- fecha del articulo -->
      <p class="text-capitalize pt-0 pt-sm-2 text-center text-sm-left">
        <?php while ( have_posts() ) : the_post(); ?>
          <?php the_modified_date(); ?>
        <?php endwhile;?>
      </p>
    </div>
  </div>
  <!-- slide -->
  <div class="d-print-none">
    <?php $slide = get_field('slide'); ?>
    <?php if($slide): ?>
      <?php echo do_shortcode($slide);?>
    <?php endif; ?>
  </div>
  <div class="container paragraph pt-0">
    <!-- links sociales -->
    <div class="px-4 pt-4 d-print-none">
      <div class="article-share d-flex justify-content-center justify-content-sm-end">
        <a href="mailto:?Subject=<?php the_title(); ?>&Body=<?php the_permalink(); ?>">
          <i class="fa fa-envelope-o text-gray"></i>
        </a>
        <a title="Imprimir articulo" 
          href="javascript:void(0)" 
          onclick="window.print()">
          <i class="fa fa-print text-gray"></i>
        </a>
        <a target="_blank" 
          href="http://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>">
          <i class="fa fa-twitter twitter"></i>
        </a>
        <a target="_blank" 
          href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>">
          <i class="fa fa-facebook-square facebook"></i>
        </a>
      </div>
    </div>
    <hr class="d-print-none">
    <!-- contenido -->
    <div id="content">
      <p class="article px-4 pb-5">
        <?php while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile;?>
      </p>
    </div>
  </div>
</section>

<?php get_footer(); ?>
