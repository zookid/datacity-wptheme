<?php
/**
 * Template Name: participamos_lista
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
global $post;

?>
<section id="top">
    <?php get_template_part( 'global-templates/topnav' ); ?>
</section>


<?php

$destacados = $post->post_name == "destacadas";

$participamos_query = new WP_Query( array( 
  'category_name' => 'participamos',  
  'tag' => $destacados ? 'destacado' : '',
  'order' => 'DESC',
));
?>

<section id="participamos">
  <h1 class="bg-blue">Participamos</h1>
  <h3>
    <a class="<?php echo $post->post_name == "participamos" ? 'active' : ''; ?>" 
      href="<?php echo get_page_link( get_page_by_path( 'participamos' ) ); ?>">
      - TODAS
    </a>
    <a class="<?php echo $post->post_name == "destacadas" ? 'active' : ''; ?>" 
      href="<?php echo get_page_link( get_page_by_path( 'participamos/destacadas' ) ); ?>">
      - DESTACADAS -
    </a>
  </h3>
  <div id="participamos-list" class="container-fluid">
    <div class="row h-100">
      <?php if ( $participamos_query->have_posts() ) : ?>
        <?php $n = 0;
        while ( $participamos_query->have_posts() ) : $participamos_query->the_post(); ?>
          <div class="col-md-4 thumb <?php echo $n %2 == 0 ? 'ph-lightgray' : 'ph-gray'; ?>"
          style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full')[0]; ?>)">
            <a href="<?php echo the_permalink(); ?>">
              <?php the_title(); ?>
            </a>
          </div>
        <?php $n++; endwhile; ?>
        <?php wp_reset_postdata(); ?>
      <?php else: ?>
        <h3 class="mx-auto text-gray">
          No hay proyectos que mostrar
        </h3>
      <?php endif; ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>