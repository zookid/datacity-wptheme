<?php
/**
 * Template Name: contacto
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>
<section id="top">
    <?php get_template_part( 'global-templates/topnav' ); ?>
</section>

<section id="contacto">
  <div class="header bg-black">
    <h1>Contacto</h1>
    <span>
      - SOLICITA NUESTRAS SOLUCIONES -
    </span>
  </div>
  <div class="map-world"></div>
  <div id="map" class="bg-lightgray"></div>
  <div class="container paragraph">
    <h4 class="text-lightblue kelsonBold px-2">
      - CHILE -
    </h4>
    <hr>
    <div class="d-flex pl-4">
      <div class="mr-2">
        <img class="mt-2" src="<?php echo get_template_directory_uri(); ?>/img/marker.png">
      </div>
      <div class="w-100">
        <b class="kelsonBold align-top">
          SANTIAGO
        </b>
        <p class="m-0">
          AV. ECHEÑIQUE 5839, OF 512, LA REINA,
          SANTIAGO, REGI&Oacute;N METROPOLITANA
        </p>
        <div class="d-md-flex justify-content-between">
          <div id="telefono" class="kelsonBold d-flex">
            <div class="pt-2 pt-md-0 pr-1">
              <img src="<?php echo get_template_directory_uri(); ?>/img/icono-phone-alt.png" 
                class="navbar-icon" 
                alt="Telefono">
            </div>
            <div>
              <div class="mr-2 mr-md-0 d-block d-md-inline">
                +562 29935452
              </div>
              <div class=" d-block d-md-inline">
                +562 29935453
              </div>
            </div>
          </div>
          <div class="tag mt-2 mt-md-0">
            <a class="nostyle" href="mailto:INFO@DATACITYIOT.COM">
              INFO@DATACITYIOT.COM
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>