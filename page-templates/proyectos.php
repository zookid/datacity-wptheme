<?php
get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<section id="top" class="d-print-none">
  <?php get_template_part( 'global-templates/topnav' ); ?>
</section>

<section>
  <h1 class="bg-black">
    <a class="nostyle" href="<?php echo get_page_link(get_page_by_path('proyectos'));?>">Proyectos</a>
  </h1>
  <!-- titulo y subtitulo -->
  <h3 class="bg-lightgray text-white subtitle-nav">
    <div class="subtitle-main">
      <?php while ( have_posts() ) : the_post(); ?>
        <?php the_title(); ?>
      <?php endwhile;?>
      <div class="subtitle-caption text-lightblue">
        <?php if(get_field('subtitulo')): ?>
          <?php the_field('subtitulo');?>
        <?php endif; ?>
      </div>
    </div>
    <!-- navegacion entre posts -->
    <span class="xx d-print-none container-fluid">
      <div class="row">
        <div class="col text-left">
          <?php previous_post_link( '%link', '< PROYECTO ANTERIOR', TRUE ); ?>
        </div>
        <div class="col text-right">
          <?php next_post_link( '%link', 'SIGUIENTE PROYECTO >', TRUE ); ?>
        </div>
      </div>
    </span>
  </h3>
  <!-- slide -->
  <div class="d-print-none">
    <?php $slide = get_field('slide'); ?>
    <?php if($slide): ?>
      <?php echo do_shortcode($slide);?>
    <?php endif; ?>
  </div>
  <!-- descripcion del proyecto -->
  <div class="container paragraph">
    <h4 class="px-4">DESCRIPCI&Oacute;N DEL PROYECTO</h3>
    <div class="row px-4">
      <div class="col-lg d-sm-flex mb-3 mb-lg-0 text-center text-sm-left">
        <!-- logo del cliente -->
        <div class="proyecto-logo-cliente mr-0 mr-sm-3">
          <?php $logo = get_field('logo_empresa'); ?>
          <?php if ($logo): ?>
            <img src="<?php echo $logo['url']; ?>" alt="logo de la empresa" />
          <?php endif; ?>
        </div>
        <div>
          <!-- detalles -->
          <span class="text-uppercase">
            CLIENTE:
            <?php if (get_field('cliente')): ?>
              <?php the_field('cliente'); ?>
            <?php endif; ?>
          </span>
          <span class="text-uppercase">
            TIPO DE PROYECTO:
            <?php if (get_field('tipo_proyecto')): ?>
              <?php the_field('tipo_proyecto'); ?>
            <?php endif; ?>
          </span>
          <span class="text-uppercase">
            LOCACI&Oacute;N:
            <?php if (get_field('locacion')): ?>
              <?php the_field('locacion'); ?>
            <?php endif; ?>
          </span>
          <span>
            FECHA: 
            <?php while ( have_posts() ) : the_post(); ?>
              <?php the_modified_date(); ?>
            <?php endwhile;?>
          </span>
        </div>
      </div>
      <div class="col-lg-3 d-print-none">
        <!-- flechita (condicionar) -->
        <div class="pointer-wrapper">
          <div class="pointer-tip pointer-success"></div>
          <div class="pointer bg-tag-success">
            NUEVO PROYECTO
          </div>
        </div>
        <!-- links sociales, imprimir -->
        <div class="article-share d-flex justify-content-center justify-content-lg-end mt-4">
          <a href="mailto:?Subject=<?php the_title(); ?>&Body=<?php the_permalink(); ?>">
            <i class="fa fa-envelope-o text-gray"></i>
          </a>
          <a title="Imprimir articulo" 
            href="javascript:void(0)" 
            onclick="window.print()">
            <i class="fa fa-print text-gray"></i>
          </a>
          <a target="_blank" 
            href="http://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>">
            <i class="fa fa-twitter twitter"></i>
          </a>
          <a target="_blank" 
            href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>">
            <i class="fa fa-facebook-square facebook"></i>
          </a>
        </div>
      </div>
    </div>
    <hr>
    <!-- contenido -->
    <div id="content">
      <p class="article px-4 pb-5">
        <?php while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile;?>
      </p>
    </div>
  </div>
</section>

<?php get_footer(); ?>
