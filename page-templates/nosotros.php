<?php
/**
 * Template Name: nosotros
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<section id="top">
  <?php get_template_part( 'global-templates/topnav' ); ?>
</section>

<section>
  <h1 class="bg-gray">Datacity IoT</h1>
  <!-- slider -->
  <div class="d-print-none">
    <?php $slide = get_field('slide'); ?>
    <?php if($slide): ?>
      <?php echo do_shortcode($slide);?>
    <?php endif; ?>
  </div>
  <div id="nosotros" class="container paragraph pt-0 mb-5">
    <!-- links sociales -->
    <div class="px-4 pt-4">
      <div class="article-share d-flex justify-content-center justify-content-sm-end d-print-none">
        <a href="mailto:?Subject=<?php the_title(); ?>&Body=<?php the_permalink(); ?>">
          <i class="fa fa-envelope-o text-gray"></i>
        </a>
        <a title="Imprimir articulo" 
          href="javascript:void(0)" 
          onclick="window.print()">
          <i class="fa fa-print text-gray"></i>
        </a>
        <a target="_blank" 
          href="http://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>">
          <i class="fa fa-twitter twitter"></i>
        </a>
        <a target="_blank" 
          href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>">
          <i class="fa fa-facebook-square facebook"></i>
        </a>
      </div>
    </div>
    <hr class="mt-4 mb-2 s-print-none">
    <!-- texto de introduccion -->
    <h4 class="px-4 pt-4">ACERCA DE DATACITY IoT</h3>
    <p class="article-text px-4 pb-4">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
        the_content();
        endwhile; else: ?>
        <p>Datacity IoT</p>
      <?php endif; ?>
    </p>
    <hr class="mt-4 mb-2 d-print-none">
    <h4 class="px-4 pt-4 pb-3">NUESTRO EQUIPO</h3>
    <!-- integrentes del equipo -->
    <?php
      $equipo_query = new WP_Query( array(
        'category_name' => 'equipo',
        'orderby'  => array( 'meta_value_num' => 'ASC' ),
        'meta_key' => 'orden'
      ));
    ?>
    <div class="row px-4">
      <?php if ( $equipo_query->have_posts() ) : ?>
      <?php $n = 0;
        while ( $equipo_query->have_posts() ) : $equipo_query->the_post(); ?>
        <div class="col-sm-6 col-lg-3 mb-4">
          <div class="profile">
            <!-- foto de perfil -->
            <div class="profile-pic">
              <?php $perfil = get_field('integrante_imagen'); ?>
              <?php if ($perfil): ?>
                <img src="<?php echo $perfil['url']; ?>" alt="Imagen del integrante" />
              <?php else: ?>
                <img src="http://via.placeholder.com/175" alt="">
              <?php endif; ?>
            </div>
            <!-- nombre -->
            <h5 class="mt-2 text-capitalize">
              <?php if(get_field('integrante_nombre')): ?>
                <?php the_field('integrante_nombre'); ?>
              <?php endif; ?>
            </h5>
            <!-- cargo del integrante -->
            <h6>
              <?php if(get_field('integrante_cargo')): ?>
                <?php the_field('integrante_cargo'); ?>
              <?php endif; ?>
            </h6>
            <!-- links sociales -->
            <b>
              <?php $correo = get_field('integrante_correo'); ?>
              <?php if ($correo): ?>
                <a class="nostyle" href="mailto:<?php echo $correo ?>">
                  <?php echo $correo ?>
                </a>
              <?php endif; ?>
            </b>
            <div class="article-share pt-0">
              <!-- twitter -->
              <?php $twitter = get_field('twitter'); ?>
              <?php if ($twitter): ?>
                <a href="https://twitter.com/<?php echo $twitter ?>" target = "_blank">
                  <i class="fa fa-twitter twitter"></i>
                </a>
              <?php else: ?>
                <a href="#">
                  <i class="fa fa-twitter twitter"></i>
                </a>
              <?php endif; ?>
              <!-- facebook -->
              <?php $facebook = get_field('facebook'); ?>
              <?php if ($facebook): ?>
                <a href="https://facebook.com/<?php echo $facebook ?>" target = "_blank">
                  <i class="fa fa-facebook-square facebook"></i>
                </a>
              <?php else: ?>
                <a href="#">
                  <i class="fa fa-facebook-square facebook"></i>
                </a>
              <?php endif; ?>
              <!-- linkedin -->
              <?php $linkedin = get_field('linkedin'); ?>
              <?php if ($linkedin): ?>
                <a href="https://linkedin.com/in/<?php echo $linkedin ?>" target = "_blank">
                  <i class="fa fa-linkedin-square facebook"></i>
                </a>
              <?php else: ?>
                <a href="#">
                  <i class="fa fa-linkedin facebook"></i>
                </a>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <?php $n++; endwhile; ?>
        <?php wp_reset_postdata(); ?>
      <?php else: ?>
        <p class="mx-auto text-gray">
          No se han agregado miembros al equipo aún.
        </p>
      <?php endif; ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>